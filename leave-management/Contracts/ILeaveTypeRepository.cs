﻿using LeaveManagement.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LeaveManagement.Contracts {
    public interface ILeaveTypeRepositoryAsync : IRepositoryBaseAsync<LeaveType, int> {

    }
}
